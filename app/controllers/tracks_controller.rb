class TracksController < ApplicationController
  def new
    @track = Track.new
    @track.wallets.build
  end
  
  def create
    @track = Track.new(track_params)
    if human?(@track) && @track.save
      flash[:success] = 'Bookmark this page to easily access it in the future!'
      redirect_to "/#{@track.token}"
    else
      render action: 'new'
    end
  end

  def edit
    @track = Track.find_by_token params[:id]
    redirect_to :root unless @track.present?
  end
  
  def update
    @track = Track.find_by_token params[:id]
    if @track.update_attributes(track_params)
      flash[:success] = 'Update successful!'
      redirect_to "/#{@track.token}"
    else
      render action: 'edit'
    end
  end

  def show
    @track = Track.find_by_token params[:id]
    redirect_to :root unless @track.present?
  end
  
  def destroy
    track = Track.find_by_token params[:id]
    track.destroy
    flash[:success] = 'Deleted!'
    redirect_to :root
  end

  def summary
    @track = Track.find_by_token params[:id]
    render layout: false
  end
  
  private
  
  def track_params
    params.require(:track).permit(:currency_id,
      wallets_attributes: [:id, :address, :description, :_destroy])
  end
  
  def human? model
    Rails.env.development? || verify_recaptcha(model: model)
  end
end
